package selenium;
/*
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import automatedIntelligence.SmartEasyPlayer;
import game.Card;
import game.GameBoard;
import game.Tableau;
import resources.Move;
*/
public class SeleniumEasyGame/* extends SeleniumGame */{
	/*
	GameBoard copyBoard;
	static boolean debug;
	
	public static void main(String[] args){
		//debug = true;
		SeleniumEasyGame testGame = new SeleniumEasyGame();
		
		testGame.sgb = new GameBoard();
		
		
		
		testGame.onlineStart();
		testGame.sgb.dealEasyGame();
		
		testGame.onlineDeal();
		testGame.copyBoard = new GameBoard(testGame.sgb);
		SmartEasyPlayer.startGame(testGame.sgb);
		SmartEasyPlayer.play();
		
		testGame.sgb = testGame.copyBoard;
		
		testGame.makeOnlineMoves(SmartEasyPlayer.moveHistory);
		
		testGame.sgb.printGame();
		
	}
	
	
	
	WebDriver driver;
	@Override
	public void startGame() {
		sgb = new GameBoard();
		onlineStart();
		
		sgb.dealEasyGame();
		onlineDeal();
		
		
		SmartEasyPlayer.startGame(sgb);
	}
	
	public void onlineStart(){
		System.setProperty("webdriver.chrome.driver", "C:\\ChromeDriver\\chromedriver.exe");
		
		driver = new ChromeDriver();
		driver.get("https://www.solitr.com/spider-solitaire");
		
		
	}
	
	public void onlineDeal(){
		List<WebElement> cardList = driver.findElements(By.xpath("//*[@id=\"solitaireCanvas\"]/div[2]/*[@class=\"card\"]"));
		for(int i=44-1; i>=0; i--){
			Card thisCard = sgb.columns.get(i%10).tableau.get(i/10);
			assignWebCard(thisCard, cardList.remove(0));
		}
		for(int i=9; i>=0; i--){
			Card thisCard = sgb.columns.get(i).getTopCard();
			assignWebCard(thisCard, cardList.remove(0));
		}

		for(Card[] i: sgb.deals){
			for(Card j: i){
				assignWebCard(j, cardList.remove(0));
			}
		}
		
	}
	
	private void assignWebCard(Card thisCard, WebElement onlineCard){
		thisCard.webID = onlineCard.getAttribute("id");
		thisCard.value= Integer.valueOf(thisCard.webID.substring(2))%13+1;
	}
	
	
	@Override
	public void dealout() {
		
		
		WebElement element = driver.findElement(By.id(sgb.deals.get(0)[0].webID));

		Actions actions = new Actions(driver);

		actions.moveToElement(element).click().perform();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	
	}
	
	public void makeOnlineMoves(List<Move> moveHistory){
		for(Move i: moveHistory){
			if(debug){
				try{
					System.in.read();System.in.read();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			if(i!= null){
				System.out.print("Move "+i.count+" from "+i.origin+" to "+i.destination);
				if(sgb.columns.get(i.destination).tableau.size()>0) System.out.println(" ("+sgb.columns.get(i.origin).tableau.get(sgb.columns.get(i.origin).tableau.size()-i.count).value+" to "+sgb.columns.get(i.destination).getTopCard().value+")");
				else System.out.println(" ("+sgb.columns.get(i.origin).tableau.get(sgb.columns.get(i.origin).tableau.size()-i.count).value+" to empty cell");
				makeMove(i.count, i.origin, i.destination);
			}
			else{
				dealout();
				sgb.dealNext();
			}
			
		}
	}

	@Override
	public void makeMove(int numCards, int origin, int destination) {
		int doneCount = sgb.completedStacks;
		Tableau originT = sgb.columns.get(origin);
		Tableau destinationT = sgb.columns.get(destination);
		originT.process();
		originT.process();
		boolean needsToFlip = originT.distanceToUnflipped == 1 && originT.distanceToEmpty>1;
		WebElement from = driver.findElement(By.id(originT.tableau.get(originT.tableau.size()-numCards).webID));
		WebElement to = null;
		if(destinationT.tableau.size()>0){
			to = driver.findElement(By.id(destinationT.getTopCard().webID));
		}
		else{
			List<WebElement> basesList= driver.findElements(By.id("tableauPileBase"+(destination)));
			System.out.println(destination);
			if(basesList.size()!=1){
				class WebFinderException extends RuntimeException{};
				throw new WebFinderException();
			}
			
			to = basesList.get(0);
		}
		System.out.println(to.getAttribute("id"));
		new Actions(driver).moveToElement(from, 15, 5).clickAndHold().moveToElement(to, 15, 15).release().perform();
		sgb.moveCards(numCards, origin, destination);
		try {
			Thread.sleep(60);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if(doneCount!=sgb.completedStacks){
			try {
				Thread.sleep(1100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(needsToFlip){
			try{Thread.sleep(200);}
			catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean assertVictory() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyBoard() {
		
		return false;
	}

	@Override
	public void rescanBoard() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean runGame() {
		boolean win = SmartEasyPlayer.play();
		makeOnlineMoves(SmartEasyPlayer.moveHistory);
		
		return win;
	}
	*/
}
