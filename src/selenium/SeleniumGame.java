package selenium;

import game.GameBoard;
import game.Tableau;

public abstract class SeleniumGame {
	public GameBoard sgb;
	public abstract void startGame();
	
	/**Returns whether the player was victorious**/
	public abstract boolean runGame();
	public abstract void dealout();
	public abstract void makeMove(int numCards, int origin, int destination);
	public abstract boolean assertVictory();
	public abstract boolean verifyBoard();
	public abstract void rescanBoard();
}
