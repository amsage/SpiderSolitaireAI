package resources;

import java.util.ArrayList;
import java.util.HashSet;

import automatedIntelligence.SmartEasyPlayer;
import automatedIntelligence.SmartEasyPlayer.State;
import game.GameBoard;
import game.Tableau;

public class VirtualBoard {
	GameBoard board = null;
	public ArrayList<MoveTree> moves= new ArrayList<>();
	public double entropy;
	public static boolean debug;
	public static boolean debugInfinity;
	public SmartEasyPlayer player;
	
	public VirtualBoard(GameBoard copyBoard, SmartEasyPlayer newPlayer){
		board = new GameBoard(copyBoard);
		this.player = newPlayer;
	}

	public void calculateEntropy() {
		
		entropy = 0.0d;
		HashSet<Integer> values = new HashSet<>();
		
		double sumOfSquares = 0.0;
		double squareOfSums = 0.0;
		
		for(Tableau i: board.columns){
			i.process();
			
			sumOfSquares+=i.distanceToEmpty*i.distanceToEmpty;
			squareOfSums+=i.distanceToEmpty;
			
			if(i.containsKing()){
				entropy+=5.0;
				if(i.distanceToEmpty==1){
					entropy+=5.0;
				}
			}
			for(int x=1; x<=i.unflippedCards; x++){
				entropy+=4.0/x;
				
			}
			for(int x=1; x<=i.distanceToEmpty-1; x++){
				entropy+=0.5/x;
			
			}
			
			
			
			if(i.tableau.size()==0){
				entropy-=6.0;
			
			}
			else{
				
				if(i.getTopCard().getValue()>1 && i.getTopCard().isFaceUp()==true){
					values.add(i.getTopCard().getValue());
				}
			}
			
		}
		
		squareOfSums/=10;
		squareOfSums*=squareOfSums;
		
		double variance = sumOfSquares/10.0-squareOfSums;
		
		entropy+=variance*0.2;
		
		entropy-=values.size()*0.02;
		
		if(player.state==SmartEasyPlayer.State.DEALING){
			adjustDealingEntropy();
		}
		
		
	}

	public void virtualMove(Move move) {
		board.moveCards(move, false);		
	}

	public MoveTree getBestMove() {
		MoveTree best = null;
		double bestEntropy=entropy;
		
		for(MoveTree i: moves){
			if(i.score<bestEntropy){
				bestEntropy = i.score;
				best = i;
			}
		}
		
		return best;
	}

	public boolean hasGoodMoves() {
		for(MoveTree i: moves){
			if(i.score<entropy){
				return true;
			}
		}
		
		return false;
	}
	
	public boolean checkRevealedCards(){
		for(Tableau i: board.columns){
			if(i.tableau.size()>0 && i.getTopCard().isFaceUp()==false){
				return true;
			}
		}
		
		return false;
	}

	public static double getScore(VirtualBoard plan) {
		
		
		if(plan.player.plansList.contains(new MovePlan(plan.board))){
			if(debugInfinity)System.out.println(plan.player.plansList.size());
			return 1_000_000;
		}
		
		if(plan.player.plansList.size()>1000){
			return 1000;
		}
		
		
		plan.player.plansList.add(new MovePlan(plan.board));
		
		if(plan.board.cardCount()<10&&plan.board.cardCount()!=0){
			return 5_000_000;
		}
		
		double score = 1e10;
		VirtualBoard nextPlan = null;
		int cardsMoved = 0;
		
		for(int i=0; i<10; i++){
			plan.board.columns.get(i).process();
			
			for(Tableau j:plan.board.columns.get(i).perfectTargets){
				int dest = plan.board.columns.indexOf(j);
				nextPlan = new VirtualBoard(plan.board, plan.player);
				cardsMoved = nextPlan.board.fullMove(i, dest, false);
				nextPlan.calculateEntropy();
				if(debug)System.out.println("Virtual move:"+cardsMoved+" "+i+" "+dest);
				if(nextPlan.checkRevealedCards()){
					score = nextPlan.entropy;
					
					if(plan.player.state==State.DEALING){
						score-=1000.0;
					}
					else{
						score-=6.0;
					}
				}
				else{
					score = getScore(nextPlan);
				}
				
				
				MoveTree newTree = new MoveTree(new Move(cardsMoved, i, dest), score, plan.board, plan.player);
				newTree.board.moves = nextPlan.moves;
				plan.moves.add(newTree);
				
				
				
			}
			for(Integer j:plan.board.columns.get(i).perfectHalfTargets)
			{
				//breakpoint
				nextPlan = new VirtualBoard(plan.board, plan.player);
				cardsMoved = nextPlan.board.halfMove(i,j);
				nextPlan.calculateEntropy();
				if(debug)System.out.println("Virtual half-move:" +cardsMoved+" "+i+" "+j);
				score = getScore(nextPlan);
				MoveTree newTree = new MoveTree(new Move(cardsMoved, i, j), score, plan.board, plan.player);
				newTree.board.moves = nextPlan.moves;
				plan.moves.add(newTree);
			}
			
		}
		
		
		
		MoveTree a = plan.getBestMove();
		if(a!=null){
		return a.score;}
		else{
			return plan.entropy;
		}
		
	}

	public void planDealingMoves() {
		// TODO Auto-generated method stub
		
	}

	public void adjustDealingEntropy() {
		if(board.columns.stream().noneMatch(a->a.tableau.size()==0)){
			entropy-=1000;
		}
		
	}

	
	
	
}
