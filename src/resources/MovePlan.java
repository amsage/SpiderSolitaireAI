package resources;

import java.util.ArrayList;
import java.util.Iterator;

import game.Card;
import game.GameBoard;
import game.Tableau;

public class MovePlan implements Comparable<MovePlan>{
	ArrayList<Card> table = new ArrayList<>(100);
	int stacksMade;
	
	MovePlan(GameBoard planBoard){
		stacksMade = planBoard.completedStacks;
		for(Tableau i:planBoard.columns){
			table.addAll(i.tableau);
			table.add(null);
		}
	}
	
	@Override
	public int compareTo(MovePlan other){
		int comp = 0;
		Iterator<Card> thisOne = this.table.iterator();
		Iterator<Card> thatOne = other.table.iterator();
		
		while(comp==0 && thisOne.hasNext() && thatOne.hasNext()){
			Card thisCard = thisOne.next();
			Card thatCard = thatOne.next();
			
			if(thisCard != null && thatCard != null) comp = thisCard.compareTo(thatCard);
		}
		
		if(thisOne.hasNext() && !thatOne.hasNext()){
			return -1;
		}
		else if(!thisOne.hasNext() && thatOne.hasNext()){
			return 1;
		}
		
		
		
		return comp;
	}
	
	@Override
	public boolean equals(Object other){
		if(!(other instanceof MovePlan)){
			return false;
		}
		
		MovePlan otherMP = (MovePlan)other;
		
		return this.stacksMade==otherMP.stacksMade && this.table.equals(otherMP.table);
	}
	
	@Override
	public int hashCode(){
		return stacksMade+table.hashCode();
	}
	
}
