package resources;

import java.util.ArrayList;

import game.Card;
import game.GameBoard;
import game.Tableau;

public class ReversableBoard {
	ArrayList<Tableau> columns = new ArrayList<>(10);
	int stacksMade;
	ArrayList<Card[]> deals= new ArrayList<>(5);
	
	public ReversableBoard(GameBoard copyBoard){
		for(Tableau i: copyBoard.columns){
			this.columns.add(new Tableau(i, i.getOwner()));
		}
		stacksMade = copyBoard.completedStacks;
		for(Card[] i : copyBoard.deals){
			Card[] dealStack = new Card[10];
			deals.add(dealStack);
			for(int j=0; j<10; j++){
				dealStack[j]=i[j];
			}
		}
	}
	
	public void dealNext() {
		Card[] newDeal = deals.remove(0);
		for (int i = 0; i < 10; i++) {
			columns.get(i).tableau.add(newDeal[i]);
		}//Warning!  Does not check for completed stack!
	}//TODO:  Check for completed stack
	
	
	public void undeal(){
		Card[] newUndeal = new Card[10];
		deals.add(0, newUndeal);
		for(int i=0; i<10; i++){
			Tableau iTableau = columns.get(i);
			newUndeal[i] =iTableau.getTopCard();
			iTableau.tableau.remove(iTableau.tableau.size()-1);
		}
	}
	
	public boolean isReadyToDeal(){
		if(deals.size()<=0){
			return false;
		}
		
		for(Tableau i: columns){
			if(i.tableau.size()==0){
				return false;
			}
		}
		
		short s = 'A';
		
		return true;
	}
	
	
	
}
