package resources;

import automatedIntelligence.SmartEasyPlayer;
import game.GameBoard;
import game.Tableau;

public class MoveTree {
	public static boolean debug;
	public Move move = null;
	public double score = 0.0;
	public VirtualBoard board = null;
	public boolean revealing;
	
	public MoveTree(Move newMove, double newScore, GameBoard newBoard, SmartEasyPlayer player){
		move = newMove;
		score = newScore;
		board = new VirtualBoard(newBoard, player);
		board.virtualMove(move);
		board.calculateEntropy();
		
		if(debug){
			board.board.printGame();
			System.out.println(score);
		}
		
		for(Tableau i : board.board.columns){
			if(i.tableau.size()>0 && i.getTopCard().isFaceUp()==false){
				revealing = true;
			}
		}
		
	}
}
