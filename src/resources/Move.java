package resources;

public class Move {
	public int count;
	public int origin;
	public int destination;
	
	public Move(int count, int origin, int destination){
		this.count=count;
		this.origin=origin;
		this.destination=destination;
	}
}
