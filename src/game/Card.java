package game;

public enum Card {
	s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13,
	h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11, h12, h13,
	d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13,
	c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13,
	xs1, xs2, xs3, xs4, xs5, xs6, xs7, xs8, xs9, xs10, xs11, xs12, xs13,
	xh1, xh2, xh3, xh4, xh5, xh6, xh7, xh8, xh9, xh10, xh11, xh12, xh13,
	xd1, xd2, xd3, xd4, xd5, xd6, xd7, xd8, xd9, xd10, xd11, xd12, xd13,
	xc1, xc2, xc3, xc4, xc5, xc6, xc7, xc8, xc9, xc10, xc11, xc12, xc13;
	
	Card(){
		String s = this.name();
		if(s.contains("x")){
			faceUp = false;
			s=s.substring(1);
		}
		else{
			faceUp = true;
			
		}
		
		switch(s.charAt(0)){
		case 's':
			suit = Suit.SPADE;break;
		case 'h': suit = Suit.HEART;break;
		case 'd': suit = Suit.DIAMOND;break;
		case 'c': suit = Suit.CLUB;break;
		default: throw new IllegalStateException();
		}
		s=s.substring(1);
		
		value = Byte.valueOf(s);
	}
	
	private final byte value;
	private final Suit suit;
	
	
	public static enum Suit {
		SPADE, HEART, DIAMOND, CLUB;
	};
	
	
	private final boolean faceUp;
	
	

	
	
	public Card flipUp(){	
		return faceUp? this	: Card.valueOf(this.name().substring(1));
	}
	
	
	public Suit getSuit() {
		return suit;
	}

	public int getValue() {
		return value;
	}



	public boolean isFaceUp() {
		return faceUp;
	}



	
	
@Override
	public String toString() {
		int value = getValue();
		Suit suit = getSuit();

		String a;

		if (value == 1)
			a = "A";
		else if (value < 10)
			a = Integer.toString(value);
		else if (value == 10)
			a = "T";
		else if (value == 11)
			a = "J";
		else if (value == 12)
			a = "Q";
		else if (value == 13)
			a = "K";
		else
			a = "X";

		switch (suit) {
		case SPADE:
			a += '♠';
			break;
		case CLUB:
			a += '♣';
			break;
		case DIAMOND:
			a += '♦';
			break;
		case HEART:
			a += '♥';
			break;
			
			
			
		default:
			throw new NullPointerException("Card suit not found");
		}
		
		if (!faceUp) {
			a="??";
		}
		
		return a;

	}
}
