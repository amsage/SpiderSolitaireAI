package game;

import java.io.FileWriter;
import java.io.IOException;

import automatedIntelligence.DumbPlayer;
import automatedIntelligence.SmartEasyPlayer;
import resources.VirtualBoard;




public class Main implements Runnable{
	public static int winCount = 0;
	public static int gamesPlayed = 0;
	
	public static synchronized void reportGame(boolean won){
		gamesPlayed++;
		if(won) winCount++;
		System.out.println(winCount*1.0/gamesPlayed*100+"% over "+gamesPlayed+" games.");
		
	}
	
	public void run(){
		int gamesToPlay =500;
		for(int i=0; i<gamesToPlay; i++){
			SmartEasyPlayer zaren = new SmartEasyPlayer();
			GameBoard game = new GameBoard();
			game.dealEasyGame();
			
			zaren.startGame(game);
			
			boolean win = zaren.play();
			
			
			//SmartEasyPlayer.printGame();
			reportGame(win);
			try (FileWriter fw = new FileWriter("SpiderResults.txt");){
				fw.write(winCount*1.0/gamesPlayed*100+"% over "+gamesPlayed+" games.");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public static void main(String[] args) throws IOException {
		
		int numProcessors = 1;
		for(int i=0; i<numProcessors; i++){
			Thread a = new Thread(new Main());
			a.start();
		}
		
	
	}
	public static void activateDebugs(){
		SmartEasyPlayer.debug=true;
				//MoveTree.debug=true;
				//VirtualBoard.debug=true;
				VirtualBoard.debugInfinity=true;
				//SmartEasyPlayer.debugMoveTree = true;
	}
	
	public static void runTests(int numGames){
		int wins = 0;;
		int games = 0;
		int bullshitCount = 0;
		
		
		for(int i=0; i<numGames; i++){
			games++;
			
			if(runGame()){
				wins++;
			}
			if(DumbPlayer.bullshit){
				bullshitCount++;
			}
		}
		
		System.out.println((double)wins/games*100+"%");
		System.out.println(bullshitCount);
	}
	
	
	static boolean runGame(){
		GameBoard table = new GameBoard();
		DumbPlayer.resetPlayer();
		DumbPlayer.trueBoard = table;
		table.dealEasyGame();
		table.printGame();
		
		
		
		while(!table.isGameOver() && !DumbPlayer.isGameOver()){
			DumbPlayer.makeMove();
			table.printGame();
			
		}
		
		return table.isGameOver();
	}
}
