package game;

import java.util.ArrayList;

public class Tableau{
	public ArrayList<Card> tableau = new ArrayList<Card>(25);
	public int validStackSize;
	public int distanceToUnflipped;
	public int unflippedCards;
	public int distanceToEmpty;
	public ArrayList<Tableau> perfectTargets = new ArrayList<Tableau>();
	public ArrayList<Integer> perfectHalfTargets = new ArrayList<Integer>();
	public Tableau bestTarget = null;
	private GameBoard owner = null;
	
	public Tableau(GameBoard newOwner){
		owner = newOwner;
	}
	
	
	
	public Tableau(Tableau copy, GameBoard newOwner){
		for(Card i:copy.tableau){
			this.tableau.add(i);
		}
		owner = newOwner;
		tableau.equals(1);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Tableau)){
			return false;
		}
		
		Tableau a = (Tableau) obj;
		
		return this.tableau.equals(a.tableau);
	};
	
	@Override
	public int hashCode() {
		return tableau.hashCode();
	};
	
	public void setOwner(GameBoard owner){
		this.owner = owner;
	}
	
	public GameBoard getOwner(){
		return this.owner;
	}

	

	public void process() {
		unflippedCards = 0;
		for (Card i : tableau) {
			if (i.isFaceUp() == false) {
				unflippedCards++;
			}
		}

		validStackSize = getStackSize(tableau.size() - 1);

		distanceToEmpty = 0;

		int index = tableau.size() - 1;

		while (index >= 0) {
			index -= getStackSize(index);
			distanceToEmpty++;
		}

		distanceToUnflipped = distanceToEmpty - unflippedCards;

		findPerfectTargets();
		findPerfectHalfTargets();
		setEasilyPerfectlyDiggable();
	}

	public void printStats() {
		System.out.println(validStackSize + " " + distanceToUnflipped + " " + unflippedCards + " " + distanceToEmpty);
	}

	public int getStackSize(int index) {
		int stackSize = 1;
		boolean done = false;

		while (index > 0 && done == false) {
			Card current = tableau.get(index);
			Card next = tableau.get(index - 1);

			if (current.getSuit() == next.getSuit() && current.getValue() == next.getValue() - 1 && next.isFaceUp() == true) {
				stackSize++;
				index--;
			} else {
				done = true;
			}
		}
		return stackSize;
	}

	public int getStackSize() {
		return getStackSize(tableau.size() - 1);
	}

	private void findPerfectTargets() {

		perfectTargets.clear();
		if (tableau.size() == 0) {
			return;
		}

		Card deep = tableau.get(tableau.size() - getStackSize());
		Card targetCard = null;
		for (int i = 0; i < 10; i++) {
			targetCard = owner.columns.get(i).getTopCard();
			if (targetCard == null) {
				if(this.distanceToEmpty>1){
					perfectTargets.add(owner.columns.get(i));
				}
				
			} else if (targetCard.getSuit() == deep.getSuit() && targetCard.getValue() == deep.getValue() + 1) {
				perfectTargets.add(owner.columns.get(i));
			}
			
		}
	}

	private void findPerfectHalfTargets() {
		Card topCard = getTopCard();
		Card targetCard = null;
		perfectHalfTargets.clear();
		for (int i = 0; i < 10; i++) {
			targetCard = owner.columns.get(i).getTopCard();
			if (topCard != null && targetCard != null && topCard.getSuit() == targetCard.getSuit()
					&& targetCard.getValue() > topCard.getValue() && targetCard.getValue() < topCard.getValue() + getStackSize()
					&& (targetCard.getValue() + owner.columns.get(i).getStackSize() > topCard.getValue() + getStackSize()
						|| (owner.columns.get(i).isPerfectBase() && (!this.isPerfectBase()
								|| (targetCard.getValue()+owner.columns.get(i).getStackSize()==topCard.getValue()+getStackSize()
								&& distanceToEmpty>owner.columns.get(i).distanceToEmpty))))) {
				perfectHalfTargets.add(i);
			}
			
		}

	}
	
	public boolean isPerfectBase(){
		return  this.containsKing()&&this.distanceToEmpty==1;
	}
	
	public Card getDeepestCard(int index){
		return tableau.get(index-getStackSize(index)+1);
	}
	
	public Card getDeepestCard(){
		return getDeepestCard(tableau.size()-1);
	}
	
	public int getDeepestValue(int index){
		return getDeepestCard(index).getValue();
	}
	
	public int getDeepestValue(){
		return getDeepestCard().getValue();
	}
	
	public boolean EasilyPerfectlyDiggable;
	
	public void setEasilyPerfectlyDiggable(){
		if(tableau.size()==0){EasilyPerfectlyDiggable=true;}
		
		
		Card[] tops = new Card[10];
		boolean diggableSoFar = true;
		
		for(int i=0; i<10; i++){
			tops[i] = owner.columns.get(i).getTopCard();
		}
		
		int index = tableau.size()-1;
		
		while(index>=0&&tableau.get(index).isFaceUp()==true){
			boolean targetFound=false;
			for(int i=0; i<10; i++){
				if(!targetFound && owner.columns.get(i).tableau.size()!=0 && getDeepestCard(index).getValue() == tops[i].getValue()-1){
					targetFound = true;
					tops[i] = tableau.get(index);
					index-=getStackSize(index);
					
				}
			}
			
			
			if(!targetFound){
				diggableSoFar = false;
				EasilyPerfectlyDiggable=false;
				return;
			}
		}
		
		
		EasilyPerfectlyDiggable = true;
		
	}
	
	public boolean isEasilyPerfectlyDiggable(){
		return EasilyPerfectlyDiggable;
	}
	
	public boolean isEasilyPerfectlyCompletelyDiggable(){
		return isEasilyPerfectlyDiggable()&&isCompletelyRevealed();
	}
	
	private boolean isCompletelyRevealed() {
		return tableau.get(0).isFaceUp();
	}

	public boolean containsKing(){
		for(Card i: tableau){
			if(i.getValue()==13 && i.isFaceUp()){
				return true;
			}
		}
		
		return false;
	}
	

	public Card getTopCard() {
		if (tableau.size() == 0) {
			return null;
		} else
			return tableau.get(tableau.size() - 1);
	}

	public int getMaxStackupSize() {
		if(perfectTargets.isEmpty()){
			return getStackSize();
		}
		else{
			 int max = 0;
			 int x = 0;
			 for(Tableau i: perfectTargets){
				 x=i.getMaxStackupSize();
				 if(x>max){
					 max=x;
					 bestTarget = i;
				 }
			 }
			 return max+getStackSize();
		}
		
	}

	public int faceDownCardCount() {
		int faceDownCount = 0;
		for(Card i : tableau){
			if(i.isFaceUp()==false){
				faceDownCount++;
			}
		}
		return faceDownCount;
	}

	public boolean showsAce() {
		for(Card i : tableau){
			if(i.isFaceUp()&&i.getValue()==1){
				return true;
			}
		}
		return false;
	}

	public void flipUpTopCard() {
		if(this.tableau.size()>0){
			Card flipper = this.getTopCard();
			this.tableau.set(this.tableau.size()-1, flipper.flipUp());
		}
		
	}
	
	

}
