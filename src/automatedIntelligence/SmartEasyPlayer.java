package automatedIntelligence;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import game.GameBoard;
import resources.Move;
import resources.MovePlan;
import resources.MoveTree;
import resources.VirtualBoard;

public class SmartEasyPlayer {
	public ArrayList<Move> moveHistory = new ArrayList<>();
	public VirtualBoard plan;
	public Set<MovePlan> plansList= new HashSet<>();
	public GameBoard table;
	public GameBoard initialTable;
	public State state = State.NORMAL;
	public static boolean debug;
	public static boolean debugMoveTree;
	
	public enum State{NORMAL, DEALING, DESPERATE_DEALING, DONE};
	
	public void startGame(GameBoard newBoard){
		table = newBoard;
		initialTable = new GameBoard(table);
		state = State.NORMAL;
		moveHistory.clear();
	}
	
	public void printGame(){
		initialTable.printGame();
		for(Move i: moveHistory){
			if(i!=null){
				initialTable.moveCards(i, true);
				System.out.println("From "+i.origin+" to "+i.destination);
			}
			else{
				initialTable.dealNext();
				System.out.println("Dealing out");
			}
			initialTable.printGame();
		}
	}
	
	
	
	public boolean play(){
		while(state != State.DONE){
			if(state == State.NORMAL){
				makeNormalMove();
			}
			else if(state == State.DEALING){
				makeDealingMove();
			}
			else if(state == State.DESPERATE_DEALING){
				makeDesperateDealingMove();
			}
		}
		
		return table.isGameOver();
	}
	
	public void makeNormalMove(){
		plan = new VirtualBoard(table, this);
		plan.calculateEntropy();
		plansList.clear();
		
		
		VirtualBoard.getScore(plan);//recursive call!
		
		MoveTree best = null;
		
		while(plan.hasGoodMoves()){
			best = plan.getBestMove();
			if(debugMoveTree){
				System.out.println("***"+plan.moves.size());
			}
			if(debug)System.out.println("Real move (normal):"+best.move.count+" "+best.move.origin+" "+best.move.destination);
			makeMove(best.move);
			
			plan = best.board;
			if(debug){
				table.printGame();
			}
		}
		
		if(best== null || !best.revealing){
			state = State.DEALING;
			if(debug)System.out.println("STATE CHANGE:  NORMAL->DEALING");
		}
	}
	
	public void makeDealingMove(){
		plan = new VirtualBoard(table, this);
		plan.calculateEntropy();
		plansList.clear();
		
		VirtualBoard.getScore(plan);//recursive call!
		
		
		MoveTree best = null;
		
		while(plan.hasGoodMoves()){
			best = plan.getBestMove();
			if(debug)System.out.println("Real move (dealing):"+best.move.count+" "+best.move.origin+" "+best.move.destination);
			
			makeMove(best.move);
			plan = best.board;
			if(debug){
				table.printGame();;
			}
		}
		
		if(best!=null && best.revealing){
			state = State.NORMAL;
			if(debug)System.out.println("STATE CHANGE: DEALING->NORMAL");
			return;
		} 
		else{
			if(table.isReadyToDeal()){
				if(debug)table.printGame();
				table.dealNext();
				if(debug){
					System.out.println("DEALING OUT!");
					try {
						table.printGame();
						System.in.read();
						System.in.read();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				moveHistory.add(null);
				state = State.NORMAL;
				if(debug)System.out.println("STATE CHANGE: DEALING->NORMAL");
				return;
			}
			else{
				state = State.DESPERATE_DEALING;
				if(debug)System.out.println("STATE CHANGE: DEALING->DESP. DEALING");
				return;
			}
		}
	}
	
	public void makeDesperateDealingMove(){
		if(table.cardCount()<10){
			state = State.DONE;
			return;
		}
		
		if(table.deals.size()<=0){
			state = State.DONE;
			return;
		}
		
		Integer emptyOne = null;
		
		DEALDOWN: while(!table.isReadyToDeal()){
			for(int i=0; i<10; i++){
				if(table.columns.get(i).tableau.size()==0){
					emptyOne = i;
				}
			}
			for(int i=0; i<10; i++){
				if(table.columns.get(i).tableau.size()>1){
					makeMove(new Move(1, i, emptyOne));
					continue DEALDOWN;
				}
			}
		}
		
		moveHistory.add(null);
		table.dealNext();
		state = State.NORMAL;
		if(debug)System.out.println("STATE CHANGE: DESP. DEALING->NORMAL");
		
	}
	
	private void makeMove(Move move){
		moveHistory.add(move);
		if(debug)table.printGame();
		table.moveCards(move, true);
		if(debug){
			try {
				table.printGame();
				System.in.read();
				System.in.read();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
